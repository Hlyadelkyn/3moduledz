"use strict";
const root = document.querySelector("#root");

class Card {
  requestPosts() {
    return fetch("https://ajax.test-danit.com/api/json/posts")
      .then((responce) => {
        return responce.json();
      })
      .catch((e) => {
        console.log(e.message);
      });
  }
  requestUsers() {
    return fetch("https://ajax.test-danit.com/api/json/users")
      .then((responce) => {
        return responce.json();
      })
      .catch((e) => {
        console.log(e.message);
      });
  }

  deleteCard(event) {
    document.querySelector(`#card${event.currentTarget.id}id`).remove();
    fetch(
      "http://ajax.test-danit.com/api/json/posts/" + event.currentTarget.id,
      {
        method: "DELETE",
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
      }
    );
  }

  render(cardId) {
    this.requestPosts().then((posts) => {
      let li = document.createElement("li");
      li.classList.add("post");
      let deletePostBtn = document.createElement("button");
      deletePostBtn.classList.add("post__delete-btn");
      deletePostBtn.innerText = "delete";
      deletePostBtn.id = cardId;
      deletePostBtn.addEventListener("click", this.deleteCard);

      this.requestUsers().then((authors) => {
        try {
          let { name, email } = authors.find((author) => {
            return author.id === posts[cardId].userId;
          });
          let postAuthorName = document.createElement("p");
          postAuthorName.classList.add("post__author-name");
          postAuthorName.innerText = name;

          let postAuthorEmail = document.createElement("p");
          postAuthorEmail.classList.add("post__author-email");
          postAuthorEmail.innerText = email;
          let postText = document.createElement("p");
          postText.classList.add("post__body");
          postText.innerText = posts[cardId].body;

          let postTitle = document.createElement("h2");
          postTitle.classList.add(".post__title");
          postTitle.innerText = posts[cardId].title;

          li.append(postTitle);
          li.append(postAuthorName);
          li.append(postAuthorEmail);
          li.append(postText);
          li.append(deletePostBtn);
          li.id = `card${cardId}id`;

          document.querySelector("#root > ul").append(li);
        } catch (error) {
          console.log(error.message);
        }
      });
    });
  }
}

class Cards {
  request() {
    {
      return fetch("https://ajax.test-danit.com/api/json/posts")
        .then((responce) => {
          return responce.json();
        })
        .catch((e) => {
          console.log(e.message);
        });
    }
  }
  render() {
    let postsList = document.createElement("ul");
    postsList.classList.add("posts-list");
    document.querySelector("#root").append(postsList);

    this.request().then((data) => {
      for (const i in data) {
        new Card().render(data[i].id);
      }
    });
  }
}
new Cards().render();
