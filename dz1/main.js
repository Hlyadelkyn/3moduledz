"use strict";

//прототипне наслідування - наслідування що відбується без участі классів, тобто тільки об'єктами.

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  getName() {
    return this.name;
  }
  setName(newName) {
    this.name = newName;
  }
  getAge() {
    return this.age;
  }
  setAge(newAge) {
    this.age = newAge;
  }
  getSalary() {
    return this.salary;
  }
  setSalary(newSalary) {
    this.salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  getSalary() {
    return this.salary;
  }
  setSalary(newSalary) {
    this.salary = newSalary;
  }
}

const prog1 = new Programmer("Vasya", 8, 3000, "js");
const prog2 = new Programmer("Ivan", 21, 230, "php");

prog1.setSalary(10);
console.log(prog1);
console.log(prog2);
