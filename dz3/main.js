"use strict";

// Декструктуризація потрібна для спрощення, оптимізації і чистки коду. це "витягування" властивостей (з об'єкта) або елементів (з массиву).
//______________________TASK1_________________
const clients1 = [
  "Гилберт",
  "Сальваторе",
  "Пирс",
  "Соммерс",
  "Форбс",
  "Донован",
  "Беннет",
];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const [, num1, , num3] = clients2;

const clients3 = [...clients1, num1, num3];
// console.log(clients3);

//______________________TASK2_________________

const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human",
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human",
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire",
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire",
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire",
  },
];

// const characters2 = characters.map((e) => {
//   delete e.gender;
//   delete e.status;
//   return e;
// });

const characters2 = characters.map(({ name, lastName, age }) => {
  let e = {};
  e.name = name;
  e.lastName = lastName;
  e.age = age;
  return e;
});
// console.log(characters2);

//______________________TASK3_________________
const user1 = {
  name: "John",
  years: 30,
};
const { name, years: age, isAdmin = "false" } = user1;
// console.log(name, age, isAdmin);

//______________________TASK4_________________
const satoshi2020 = {
  name: "Nick",
  surname: "Sabo",
  age: 51,
  country: "Japan",
  birth: "1979-08-21",
  location: {
    lat: 38.869422,
    lng: 139.876632,
  },
};

const satoshi2019 = {
  name: "Dorian",
  surname: "Nakamoto",
  age: 44,
  hidden: true,
  country: "USA",
  wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
  browser: "Chrome",
};

const satoshi2018 = {
  name: "Satoshi",
  surname: "Nakamoto",
  technology: "Bitcoin",
  country: "Japan",
  browser: "Tor",
  birth: "1975-04-05",
};

const satoshiSummary = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };
// console.log(satoshiSummary);

//______________________TASK5_________________
const books = [
  {
    name: "Harry Potter",
    author: "J.K. Rowling",
  },
  {
    name: "Lord of the rings",
    author: "J.R.R. Tolkien",
  },
  {
    name: "The witcher",
    author: "Andrzej Sapkowski",
  },
];

const bookToAdd = {
  name: "Game of thrones",
  author: "George R. R. Martin",
};
const books1 = [...books, bookToAdd];
// console.log(books1);

//______________________TASK6_________________
const employee = {
  name: "Vitalii",
  surname: "Klichko",
};
let ageTask6 = 1488;
let salaryTask6 = 0;

const employee1 = { ...employee, ageTask6, salaryTask6 };
// console.log(employee1);

//______________________TASK7_________________
const array = ["value", () => "showValue"];

// Допишіть код тут
const [value, showValue] = array;

// console.log(value); // має бути виведено 'value'
// console.log(showValue()); // має бути виведено 'showValue'
