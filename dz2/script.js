// конструкцію try catch ми можемо використовувати в тих місцях де приймаємо і обробляємо данні із-зовні, наприклад коли парсемо json, також у тих місцях де ми не впевнені у працездатності коду і його правильності функціювання.

"use strict";

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
  },
  {
    author: "Террі Пратчетт",
    price: 40,
  },
  {
    author: "Дщкувіфвіфв",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.querySelector("#root");

function render(arrToRender, root) {
  let list = document.createElement("ul");
  list.classList.add("books-list");
  let arr = arrToRender;
  arr.forEach((element) => {
    try {
      if (
        !element.hasOwnProperty("author") ||
        !element.hasOwnProperty("name") ||
        !element.hasOwnProperty("price")
      ) {
        // throw new Error("Object does not have prop");
        if (!!element.hasOwnProperty("author")) {
          throw new Error("Error: Object dsnt have author prop");
        } else if (!!element.hasOwnProperty("name")) {
          throw new Error("Error: Object dsnt have name prop");
        } else {
          throw new Error("Error: Object dsnt have price prop");
        }
      } else {
        let listElement = document.createElement("li");
        listElement.classList.add("books-list__elem");

        let authorDom = document.createElement("p");
        authorDom.classList.add("books-elem__author");
        authorDom.innerText = element.author;
        listElement.append(authorDom);

        let nameDom = document.createElement("p");
        nameDom.classList.add("books-elem__name");
        nameDom.innerText = element.name;
        listElement.append(nameDom);

        let priceDom = document.createElement("p");
        priceDom.classList.add("books-elem__price");
        priceDom.innerText = element.price;
        listElement.append(priceDom);

        list.append(listElement);
      }
    } catch (e) {
      console.log(e.message);
    }
  });

  root.append(list);
}

render(books, root);
