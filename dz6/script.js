"use strict";
let ip = "";
let status = "pending";

async function getIp() {
  try {
    let request = await fetch("https://api.ipify.org/?format=json");
    return request.json();
  } catch (e) {
    console.log(e.message);
  }
}

async function getDataJSON() {
  try {
    const responce = await getIp();
    ip = responce.ip;

    const data = await fetch(
      "http://ip-api.com/json/" +
        responce.ip +
        "?fields=status,message,continent,country,region,regionName,city,district,zip,lat,lon,mobile,query"
    );
    return data.json();
  } catch (e) {
    console.log(e.message);
  }
}

async function renderData() {
  status = "complete";
  const { city, country, continent, zip } = await getDataJSON();
  let dataArr = [continent, country, city, zip];
  document.querySelector("#root").append(ip);
  dataArr.forEach((el) => {
    let p = document.createElement("p");
    p.innerText = el + ".";
    document.querySelector("#root").append(p);
  });
}

document.querySelector("#getDataBtn").addEventListener("click", () => {
  if (status === "pending") {
    renderData();
  } else if (status === "complete") {
    document
      .querySelector("#root")
      .append("ви вже знайшли інформацію про себе.");

    status = "ended";
  }
});
