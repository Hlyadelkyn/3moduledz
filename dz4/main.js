"use strict";
// Аякс це технологія яка дозволяє взаємодіяти з сервером без оновлень сторінки, корисен в розробці лендінгів а також в створенні динамічно оновлюємих сторінкох
const url = "https://ajax.test-danit.com/api/swapi/films";
const root = document.querySelector("#root");

class Films {
  constructor(url, root) {
    this.root = root;
    this.url = url;
  }
  request() {
    return fetch(this.url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        return response.json();
      })
      .catch((e) => {
        console.log(e.message);
      });
  }
  renderCharacters({ episodeId, characters: charactersJson }) {
    const characters = charactersJson.map((element) => {
      fetch(element)
        .then((responce) => {
          return responce.json().then((data) => {
            console.log(data.name);
            let hero = document.createElement("li");
            hero.innerText = data.name;
            document.querySelector(`.film${episodeId}Heroes`).append(hero);
            return data.name;
          });
        })
        .catch((e) => {
          console.log(e.message);
        });
    });
  }
  renderFilms(filmsArr, root) {
    let filmsUl = document.createElement("ul");
    filmsArr.forEach(({ episodeId, name, openingCrawl }) => {
      let li = document.createElement("li");
      li.id = `id${episodeId}`;

      let filmId = document.createElement("p");
      filmId.innerText = episodeId;
      li.append(filmId);

      let filmName = document.createElement("h2");
      filmName.innerText = name;
      li.append(filmName);

      let filmShortText = document.createElement("p");
      filmShortText.innerText = openingCrawl;
      li.append(filmShortText);

      let heroUl = document.createElement("ul");
      heroUl.classList.add(`film${episodeId}Heroes`);
      heroUl.innerHTML = "<h3> HEROES:</h3>";
      li.append(heroUl);

      filmsUl.append(li);
    });
    root.append(filmsUl);
  }
  render(filmsArr, root) {
    this.renderFilms(filmsArr, root);
    filmsArr.forEach((film) => this.renderCharacters(film));
  }
  getFilms() {
    this.request().then((data) => {
      new Promise((resolve, reject) => {
        const filmsArr = data.map(
          ({ episodeId, name, openingCrawl, characters }) => {
            return { episodeId, name, openingCrawl, characters };
          }
        );
        if (filmsArr) {
          resolve(this.render(filmsArr, this.root));
        } else {
          reject("caput");
        }
        return filmsArr;
      });
    });
  }
}
new Films(url, root).getFilms();
